SASL Plugin for the gSignOn daemon
==================================

This plugin for the Accounts-SSO gSignOn daemon handles the SASL authentication protocol.


Build instructions
------------------

This project depends on GSignond, and uses the Meson build system. To build it, run
```
meson build --prefix=/usr
cd build
ninja
sudo ninja install
```

License
-------

See COPYING.LIB file.

Resources
---------

[SASL Plugin documentation](http://accounts-sso.gitlab.io/gsignond-plugin-sasl/index.html)

[Official source code repository](https://gitlab.com/accounts-sso/gsignond-plugin-sasl)
